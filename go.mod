module gitlab.com/AndrusGerman/go-asisten-core

go 1.15

require (
	github.com/jinzhu/gorm v1.9.16
	github.com/otiai10/copy v1.4.2
)
