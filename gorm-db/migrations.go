package db

import (
	"github.com/jinzhu/gorm"
	"gitlab.com/AndrusGerman/go-asisten-core/pkg/modasisten/models"
)

// Migrations  function
func Migrations(db *gorm.DB) error {
	return db.AutoMigrate(
		&models.KeyWord{},
		&models.GroupKey{},
		&models.Action{},
		&models.GroupKey{},
		&models.ActionResponse{},
		&models.KeywordGroup{},
		&models.ProgramsAlternative{},
		&models.CommandKey{},
	).Error
}
