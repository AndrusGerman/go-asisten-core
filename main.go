package goasistencore

import (
	"log"
	"math/rand"
	"time"

	db "gitlab.com/AndrusGerman/go-asisten-core/gorm-db"
	"gitlab.com/AndrusGerman/go-asisten-core/readconfig"
	"gitlab.com/AndrusGerman/go-asisten-core/systemgo"
	"gitlab.com/AndrusGerman/go-asisten-core/texttospeech"
)

// NewGoAsistenCore create GoAsistenCore Clients
func NewGoAsistenCore(DataDir string) (*AsistenCoreModel, error) {
	// Create New Seed for rands Actions
	rand.Seed(time.Now().Unix())
	system := systemgo.NewSystemGoModel()

	// Get Data
	modelAsisten, err := readconfig.ReadAsisten(DataDir)
	if err != nil {
		return nil, err
	}
	Db, err := db.OpenConnectionDB(DataDir, modelAsisten.Lang)
	if err != nil {
		return nil, err
	}
	db.Migrations(Db)

	// Prepare data
	speechGroup := readconfig.NewSpeech(Db)
	clientts := texttospeech.NewClient(modelAsisten.Lang)
	modelCommandKeys := readconfig.NewCommandKeyModel(Db)
	modelProgramsAlternative := readconfig.NewProgramsAlternativeModel(Db)

	// Return Asisten Core
	return &AsistenCoreModel{
		Db:                       Db,
		ModelAsisten:             modelAsisten,
		SpeechConfg:              speechGroup,
		Clientts:                 clientts,
		System:                   system,
		ModelCommandKeys:         modelCommandKeys,
		ModelProgramsAlternative: modelProgramsAlternative,
	}, nil
}

// WordsToAsistenResponse parse
func (ctx *AsistenCoreModel) WordsToAsistenResponse(words string) (string, func()) {
	if sl := ctx.SpeechConfg.Find(words); sl != nil {
		parse := ctx.Parse(words)
		// Async funtion ResponseAsisten
		var f = func() {
			go func() {
				sl.SystemGoPlay(ctx.System, parse)
				sl.RunCommandFlag(ctx.ParseCommands(sl.Command))
			}()
		}
		// Return Response
		return parse(sl.ResponseNotParse), f
	}
	// Not Response Valide
	return ctx.ModelAsisten.NotFoundGet(), func() {}
}

// WordsToSpeechBase64 convert words in speech base
func (ctx *AsistenCoreModel) WordsToSpeechBase64(words string) string {
	resp, err := ctx.Clientts.ConvertToBase64(words)
	if err != nil {
		log.Println("ERROR=WordsToSpeechBase64(1) ", err)
	}
	return resp
}

// SetSystemLog Speech Logs System
func (ctx *AsistenCoreModel) SetSystemLog(logsSystem func(text string)) {
	if ctx.System != nil {
		ctx.System.Logs = logsSystem
	}
}
