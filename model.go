package goasistencore

import (
	"strings"

	"github.com/jinzhu/gorm"
	"gitlab.com/AndrusGerman/go-asisten-core/readconfig"
	"gitlab.com/AndrusGerman/go-asisten-core/systemgo"

	"gitlab.com/AndrusGerman/go-asisten-core/texttospeech"
)

// AsistenCoreModel model
type AsistenCoreModel struct {
	ModelAsisten             *readconfig.ModelAsisten
	SpeechConfg              *readconfig.ModelSpeechGroup
	Clientts                 *texttospeech.Client
	System                   *systemgo.SystemGoModel
	ModelCommandKeys         *readconfig.ModelCommandKeys
	ModelProgramsAlternative *readconfig.ModelProgramAlternative
	Db                       *gorm.DB
	cfgDir                   string
}

// Parse Keys Speech
func (ctx *AsistenCoreModel) Parse(speech string) func(string) string {
	return func(words string) string {
		words = strings.Join(ctx.ModelProgramsAlternative.Parse(strings.Split(words, " ")), " ")
		words = ctx.ModelAsisten.Parse(words, speech)
		words = ctx.ModelCommandKeys.Parse(words)
		return words
	}
}

// ParseCommands Keys Speech
func (ctx *AsistenCoreModel) ParseCommands(command string) []string {
	if len(command) == 0 {
		return []string{}
	}
	command = ctx.ModelCommandKeys.Parse(command)
	newwords := ctx.ModelProgramsAlternative.Parse(strings.Split(command, " "))
	return newwords
}
