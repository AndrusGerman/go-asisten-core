package osfunc

import (
	"log"
	"os/exec"
	"runtime"
	"strings"
)

// CommandOutput : Run comand and return []byte output
func CommandOutput(command string) []byte {
	b, err := Setcommand(command).Output()
	if err != nil {
		log.Println("LOG:Output ", command, err)
	}
	var lenComand = len(b)
	if lenComand > 1 {
		return b[:lenComand-1]
	}
	return b
}

// CommandOutputSplit : Run comand and return []byte output
func CommandOutputSplit(command []string) []byte {
	b, err := SetcommandFlag(command).Output()
	if err != nil {
		log.Println("LOG:OutputSplit ", command, err)
	}
	var lenComand = len(b)
	if lenComand > 1 {
		return b[:lenComand-1]
	}
	return b
}

// CommandRun : Run comand and return error
func CommandRun(command string) error {
	err := Setcommand(command).Run()
	if err != nil {
		log.Println("LOG:Run ", command, err)
	}
	return err
}

// CommandRun : Run comand and return error
func CommandRuFlag(command []string) error {
	err := SetcommandFlag(command).Run()
	if err != nil {
		log.Println("LOG:RunFlag ", strings.Join(command, "|"), err)
	}
	return err
}

// Setcommand : return Cmd
func Setcommand(command string) *exec.Cmd {
	switch runtime.GOOS {
	case "windows":
		c := exec.Command(command)
		return c
	}
	return exec.Command("/bin/bash", "-c", command)
}

// SetcommandFlag : return Cmd
func SetcommandFlag(command []string) *exec.Cmd {
	switch runtime.GOOS {
	case "windows":
		c := exec.Command(command[0], command[1:]...)
		return c
	}
	var comandoNew = append([]string{"/bin/bash", "-c"}, command...)
	return exec.Command(comandoNew[0], comandoNew[1:]...)
}
