package models

// OS for find OS
type OS struct {
	ID   uint   `gorm:"primary_key"`
	Name string `gorm:"unique"`
}
