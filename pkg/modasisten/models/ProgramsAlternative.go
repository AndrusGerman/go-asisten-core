package models

import (
	"errors"
	"os"
	"runtime"
	"strings"

	"github.com/jinzhu/gorm"
)

// ProgramsAlternative model find
type ProgramsAlternative struct {
	gorm.Model
	GeneralName    string
	Programs       string
	OsID           uint
	ComandLineView bool
}

// Exits program exits
func (ctx *ProgramsAlternative) Exits() (string, error) {
	programs := strings.Split(ctx.Programs, "|")
	for _, name := range programs {
		// Verifict File is exists
		switch runtime.GOOS {
		case "windows":
			_, err := os.Open(name)
			if err == nil {
				return name, nil
			}
			continue
		}
		_, err := os.Open("/bin/" + name)
		if err == nil {
			return name, nil
		}
	}

	return "", errors.New("Not Program exits")
}
