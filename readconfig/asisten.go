package readconfig

import (
	"encoding/json"
	"io/ioutil"
	"math/rand"
	"strings"
)

// ModelAsisten model
type ModelAsisten struct {
	Lang      string
	Name      string
	SpeechKey string
	NotFound  []string
}

// ReadAsisten config
func ReadAsisten(DataDir string) (*ModelAsisten, error) {
	var model = new(ModelAsisten)
	file, err := ioutil.ReadFile(DataDir + "config/asisten.json")
	if err != nil {
		return nil, err
	}
	return model, json.Unmarshal(file, model)
}

// Parse words to asisten data
func (ctx *ModelAsisten) Parse(words string, speech string) string {
	return strings.NewReplacer(
		"{{NAMEASISTEN}}", ctx.Name,
		"{{"+ctx.SpeechKey+"}}", speech,
	).Replace(words)
}

// NotFoundGet Get
func (ctx *ModelAsisten) NotFoundGet() string {
	return ctx.NotFound[rand.Intn(len(ctx.NotFound))]
}
