package readconfig

import (
	"fmt"
	"log"
	"runtime"
	"strings"

	"github.com/jinzhu/gorm"

	"gitlab.com/AndrusGerman/go-asisten-core/osfunc"
	"gitlab.com/AndrusGerman/go-asisten-core/pkg/modasisten/models"
)

// ModelCommandKeys for db
type ModelCommandKeys struct {
	Db *gorm.DB
}

// NewCommandKeyModel New
func NewCommandKeyModel(Db *gorm.DB) *ModelCommandKeys {
	return &ModelCommandKeys{Db: Db}
}

// Parse words to comand
func (ctx *ModelCommandKeys) Parse(words string) string {
	// Get OS
	osCache.Do(func() { getOs(ctx.Db) })
	if osCache.err != nil {
		log.Println("go-asisten-core: Err ModelCommandKeys 1", osCache.err)
		return ""
	}
	// Find Programs OS
	var CommandKeys = new([]models.CommandKey)
	if err := ctx.Db.Find(CommandKeys, "os_id = ?", osCache.os.ID).Error; err != nil {
		log.Println("go-asisten-core: Err ModelCommandKeys 2", err)
		return ""
	}

	// Replace With commands
	var remplace []string
	for _, vals := range *CommandKeys {
		key := fmt.Sprintf("{{%s}}", vals.Key)
		if strings.Contains(words, key) {
			switch runtime.GOOS {
			case "windows":
				remplace = append(remplace, key, string(osfunc.CommandOutputSplit(strings.Split(vals.Command, " "))))
				continue
			}
			remplace = append(remplace, key, string(osfunc.CommandOutput(vals.Command)))
		}
	}

	// Return
	replace := strings.NewReplacer(remplace...)
	return replace.Replace(words)
}
