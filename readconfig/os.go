package readconfig

import (
	"runtime"
	"sync"

	"github.com/jinzhu/gorm"
	"gitlab.com/AndrusGerman/go-asisten-core/pkg/modasisten/models"
)

// Get OS id for run programs
func getOs(db *gorm.DB) {
	var goos = new(models.OS)
	osCache.err = db.Table("os").Find(goos, "name = ?", runtime.GOOS).Error
	osCache.os = goos
}

// Cache osDB
var osCache struct {
	os  *models.OS
	err error
	sync.Once
}
