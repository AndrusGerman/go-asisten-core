package readconfig

import (
	"fmt"
	"log"
	"strings"

	"github.com/jinzhu/gorm"

	"gitlab.com/AndrusGerman/go-asisten-core/pkg/modasisten/models"
)

// ModelProgramAlternative model
type ModelProgramAlternative struct {
	Db *gorm.DB
}

// NewProgramsAlternativeModel new model
func NewProgramsAlternativeModel(Db *gorm.DB) *ModelProgramAlternative {
	return &ModelProgramAlternative{Db: Db}
}

// Parse words to Program
func (ctx *ModelProgramAlternative) Parse(words []string) []string {
	// Get OS
	osCache.Do(func() { getOs(ctx.Db) })
	if osCache.err != nil {
		log.Println("go-asisten-core: Err ModelProgramAlternative 1", osCache.err)
		return []string{}
	}
	// Find Program
	var ProgramAlternative = new([]models.ProgramsAlternative)
	if err := ctx.Db.Find(ProgramAlternative, "os_id = ?", osCache.os.ID).Error; err != nil {
		log.Println("go-asisten-core: Err ModelProgramAlternative 2", err)
		return []string{}
	}
	// Get Programs
	var remplace []string

	for idWords := range words {
		for _, vals := range *ProgramAlternative {
			name, err := vals.Exits()
			if err != nil {
				continue
			}
			key := fmt.Sprintf("{{%s}}", vals.GeneralName)
			if strings.Contains(words[idWords], key) {
				remplace = append(remplace, key, name)
			}
		}
	}

	replace := strings.NewReplacer(remplace...)
	for idWords := range words {
		words[idWords] = replace.Replace(words[idWords])
	}
	return words
}
